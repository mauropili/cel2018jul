pragma solidity ^0.4.24;

contract Message {
    function getMessage() public view returns(string) {
        return "Hello world!";
    }
}
